// function to order array
const order = (a, b, orderBy = "ASC") => {
  const by = orderBy === "ASC" ? 1 : -1;
  return a != b ? (a > b ? by * 1 : by * -1) : 0;
};

// function to load loadouts and mount HTML
const loadLoadouts = (json) => {
  var lastType = "";

  const loadouts = json; //json.sort((a, b) => order(a.type, b.type, 'ASC'))

  return loadouts.reduce((accumulator, current) => {
    var html = accumulator;

    if (lastType != current.type) {
      html += `
                <section>
                    <h2>${current.type}s</h2>
            `;
    }

    html += `
            <h3>${current.weapon.toUpperCase()}</h3>
            <table class="styled-table">
                <thead>
                    <tr>
                        <th>Tipo</th>
                        <th>Nome</th>
                    </tr>
                </thead>
                <tbody>
                ${current.attachments.reduce((accuAtt, att) => {
                  return (
                    accuAtt +
                    `
                            <tr>
                                <td>${att.type.toUpperCase()}</td>
                                <td>${att.name.toUpperCase()}</td>
                            </tr>
                        `
                  );
                }, "")}
                </tbody>
            </table>
        `;

    if (lastType != current.type) {
      html += `
                </section>
            `;

      lastType = current.type;
    }

    return html;
  }, "");
};

const fetchJson = () => {
  fetch("./weapons.json").then((response) => {
    response.json().then((json) => {
      document.getElementById("loadouts").innerHTML = loadLoadouts(json);
    });
  });
};

fetchJson();
